package com.classpath.service;

import com.classpath.model.User;
import com.classpath.repository.UserRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class UserService {

    private final UserRepository userRepository;

    public UserService(UserRepository userRepository){
        this.userRepository = userRepository;
    }

    public User saveUser(User user){
        return this.userRepository.saveUser(user);
    }

    public Set<User> fetchAllUsers(){
        return this.userRepository.fetchAllUsers();
    }

    public User fetchUserByUserId(int userId){
        return this.userRepository
                        .fetchUserByUserId(userId)
                        .orElseThrow(UserService::invalidUser);
    }

    public void deleteUserById(int userId){
        this.userRepository.deleteUserById(userId);
    }

    public User updateUser(int userId, User user){
        System.out.println("User id is "+ userId);
       this.userRepository
                .fetchUserByUserId(userId)
                .map(u -> {
                    u.setUserId(user.getUserId());
                    u.setUsername(user.getUsername());
                    u.setEmailAddress(user.getEmailAddress());
                    return u;
                });
        return this.userRepository.fetchUserByUserId(userId).get();
    }


    private static IllegalArgumentException invalidUser() {
        return new IllegalArgumentException("Invalid user Id");
    }

}