package com.classpath.model;

public class UberPrimeDriver implements UberGo {
    @Override
    public double commute(String from, String destination) {
        System.out.println("Travelling with Harish from "+ from + " to "+ destination);
        return Math.ceil(Math.random()* 50877);
    }
}